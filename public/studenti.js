window.addEventListener("load", init);
const btnUnesi = document.querySelector('#btnUnesi');

function init() {
    loadGrupe();
}

var grupe;
function loadGrupe(){
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
        if(ajax.readyState == 4 && ajax.status == "200"){
            grupe = JSON.parse(this.responseText);
            for (var i = 0; i<grupe.length; i++) {
                var option = document.createElement("option");
                var optionText = document.createTextNode(grupe[i].naziv);
                option.appendChild(optionText);
                option.setAttribute("value", grupe[i].naziv);
                option.value = grupe[i].naziv;
                document.getElementById("selectGrupa").appendChild(option);
            }
        }
    }
    ajax.open('GET', 'http://localhost:3000/v2/grupa', true);
    ajax.send();
}

let idGrupa;
let odgovori;


btnUnesi.addEventListener("click", () => {
    // disable default action
   event.preventDefault();
   var tekst = document.getElementById("txtArea").value;
   console.log("tekst");
   var redovi = tekst.split("\n");
   var selectGrupa = document.getElementById("selectGrupa");
   var grupaNaziv = selectGrupa.options[selectGrupa.selectedIndex].text;
   for (var i = 0; i<grupe.length; i++) {
       if (grupaNaziv == grupe[i].naziv) idGrupa = grupe[i].id;
   }
   for (var j = 0; j<redovi.length; j++) {
        var headers=redovi[j].split(",");
        dodajStudenta(headers[0], headers[1], idGrupa);
   }
  
})

 

function dodajStudenta(ime, index){
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
        if(ajax.readyState == 4 && ajax.status == "200"){
            
        }
        if (ajax.readyState == 4 && ajax.status == 400){
            document.getElementById("proba").value += this.responseText;
        }
    }
    ajax.open('POST', 'http://localhost:3000/v2/student', true);
    ajax.setRequestHeader("Content-Type", "application/json");
    jsonStudent = JSON.stringify({"ime": ime, "index":index, "grupaId":idGrupa});
    ajax.send(jsonStudent);
}