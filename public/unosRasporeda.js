window.addEventListener("load", init);
const btnUnesi = document.querySelector('#btnUnesi');

function init() {
    loadPredmeti();
    loadAktivnsoti();
}

btnUnesi.addEventListener("click", () => {
     // disable default action
    event.preventDefault();
    var naziv = document.getElementById("i-naziv").value;
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
        if(ajax.readyState == 4 && ajax.status == "200"){
            postAktivnost(true);
        }
        if (ajax.readyState == 4 && ajax.status == 400){
            postAktivnost(false);
        }
    }
    ajax.open('POST', 'http://localhost:3000/v2/predmet', true);
    ajax.setRequestHeader("Content-Type", "application/json");
    jsonNaziv = JSON.stringify({"naziv": naziv});
    ajax.send(jsonNaziv);
})

function loadPredmeti(){
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
        document.getElementById("predmeti").innerHTML = this.responseText;
        
    }
    ajax.open('GET', 'http://localhost:3000/v2/predmet', true);
    ajax.send();
}

function loadAktivnsoti(){
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
            document.getElementById("aktivnosti").innerHTML = this.responseText     
    }
    ajax.open('GET', 'http://localhost:3000/v2/aktivnost', true);
    ajax.send();
}

function timeToDecimal(t) {
    var arr = t.split(':');
    var dec = parseInt((arr[1]/6)*10, 10);

    return parseFloat(parseInt(arr[0], 10) + '.' + (dec<10?'0':'') + dec);
}   

function postAktivnost(dodanPredmet) {
    var naziv = document.getElementById("i-naziv").value;
    var tipSelect = document.getElementById("i-tip");
    var tip = tipSelect.options[tipSelect.selectedIndex].text;
    var pocetak = timeToDecimal(document.getElementById("i-pocetak").value);
    var kraj = timeToDecimal(document.getElementById("i-kraj").value);
    var danSelect = document.getElementById("i-dan");
    var dan = danSelect.options[danSelect.selectedIndex].text;
    
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
        if(ajax.readyState == 4 && ajax.status == "200"){
            document.getElementById("poruka").innerHTML = this.responseText;
            loadPredmeti();
        }
        if (ajax.readyState == 4 && ajax.status == 400){
            if(dodanPredmet) {
                document.getElementById("poruka").innerHTML = this.responseText;
                deletePredmet(naziv);
            }
            document.getElementById("poruka").innerHTML = this.responseText;
        }
    }
    ajax.open('POST', 'http://localhost:3000/v2/aktivnost', true);
    ajax.setRequestHeader("Content-Type", "application/json");
    jsonNaziv = JSON.stringify({"naziv": naziv, "pocetak": pocetak, "kraj": kraj, "danId": 1, "tipId":1, "predmetId":1, "grupaId":1});
    ajax.send(jsonNaziv);
}

function deletePredmet(naziv) {
    var url = "http://localhost:3000/v2/predmet/" + naziv;
    ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (){
        if(ajax.readyState == 4 && ajax.status == "200"){
            loadPredmeti();
        }
        if (ajax.readyState == 4 && ajax.status == 400){
            loadPredmeti();
        }
    }
    ajax.open('DELETE', url, true);
   ajax.send();

}