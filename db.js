const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2017499","root","",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.predmet = sequelize.import(__dirname+'/predmet.js');
db.grupa = sequelize.import(__dirname+'/grupa.js');
db.aktivnost = sequelize.import(__dirname+'/aktivnost.js');
db.dan = sequelize.import(__dirname+'/dan.js');
db.tip = sequelize.import(__dirname+'/tip.js');
db.student = sequelize.import(__dirname+'/student.js');
db.studentGrupa = sequelize.import(__dirname+'/studentGrupa.js');

//relacije
// Predmet 1-N Grupa
db.predmet.hasMany(db.grupa,{as:'grupePredmeta'});
db.grupa.belongsTo(db.predmet, {foreignKey: "predmetId", as: "grupaPredmeta"});

// Aktivnost N-1 Predmet
db.predmet.hasMany(db.aktivnost,{as:'aktivnostiPredmeta'});
db.aktivnost.belongsTo(db.predmet, {foreignKey: "predmetId", as: "aktivnostPredmeta"});

// Aktivnost N-0 Grupa
db.grupa.hasMany(db.aktivnost,{as:'grupeAktivnosti'});
db.aktivnost.belongsTo(db.grupa, {foreignKey: "grupaId", as: "grupaAktivnosti"});

// Aktivnost N-1 Dan
db.dan.hasMany(db.aktivnost,{as:'aktivnostiDana'});
db.aktivnost.belongsTo(db.dan, {foreignKey: "danId", as: "aktivnostDana"});

// Aktivnost N-1 Tip
db.tip.hasMany(db.aktivnost,{as:'aktivnostiTipa'});
db.aktivnost.belongsTo(db.tip, {foreignKey: "tipId", as: "aktivnostTipa"});

// Student N-M Grupa
db.studentGrupa.belongsTo(db.student, {foreignKey: "studentId"});
db.studentGrupa.belongsTo(db.grupa, {foreignKey: "grupaId"});
db.grupa.belongsToMany(db.student, {through:'studentGrupa', foreignKey:'grupaId', as: 'grupeStudenti'});
db.student.belongsToMany(db.grupa, {through:'studentGrupa', foreignKey:'studentId', as: 'studentiGrupe'});



module.exports=db;