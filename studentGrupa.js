const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const StudentGrupa = sequelize.define("studentGrupa",{
        studentId:Sequelize.INTEGER,
        grupaId:Sequelize.INTEGER
    })
    return StudentGrupa;
};
