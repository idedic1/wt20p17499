const express = require('express');
const path = require("path");
const db = require('./db.js')

const app = express();
const port = 3000;

app.use(express.static(path.join(__dirname, "public")));



const {urlencoded, json} = require('body-parser');
const { studentGrupa, grupa } = require('./db.js');
app.use(json());
app.use(urlencoded({extended: false}));

// crud predmet

app.post('/v2/predmet', (req, res) => {
    const postNaziv = req.body.naziv;
    db.predmet.findOne({where:{naziv:postNaziv}}).then(function(predmet){
        if(predmet) res.send("Predmet " + req.body.naziv + " vec postoji");
        else db.predmet.create({naziv:postNaziv}).then(function() {
            res.status(200);
            res.send("Predmet " + req.body.naziv + " kreiran");
        })
    })
});

app.get('/v2/predmet', (req, res) => {
    db.predmet.findAll().then(predmeti => {
        res.json(predmeti);
    });
});

app.get('/v2/predmet/:id', (req, res) => {
    const id = req.params.id;
    db.predmet.findByPk(id).then(predmet => {
        res.json(predmet);
      });
});

app.put('/v2/predmet/:id', (req, res) => {
    const id = req.params.id;
    const updates = req.body;
    db.predmet.findByPk(id).then(rez => {
        return rez.update(updates)
      }).then(updatedPredmet => {
        res.json(updatedPredmet);
      });
});

app.delete('/v2/predmet/:id', (req, res) => {
    const id = req.params.id;
    db.predmet.destroy({
      where: { id: id }
    })
      .then(deletedPredmet => {
        res.json(deletedPredmet);
      });
});

// crud dan

app.post('/v2/dan', (req, res) => {
    const postNaziv = req.body.naziv;
    db.dan.findOne({where:{naziv:postNaziv}}).then(function(dan){
        if(dan) res.send("dan " + req.body.naziv + " vec postoji");
        else db.dan.create({naziv:postNaziv}).then(function() {
            res.status(200);
            res.send("dan " + req.body.naziv + " kreiran");
        })
    })
});

app.get('/v2/dan', (req, res) => {
    db.dan.findAll().then(dani => {
        res.json(dani);
    });
});

app.get('/v2/dan/:id', (req, res) => {
    const id = req.params.id;
    db.dan.findByPk(id).then(dan => {
        res.json(dan);
      });
});

app.put('/v2/dan/:id', (req, res) => {
    const id = req.params.id;
    const updates = req.body;
    db.dan.findByPk(id).then(rez => {
        return rez.update(updates)
      }).then(updateddan => {
        res.json(updateddan);
      });
});

app.delete('/v2/dan/:id', (req, res) => {
    const id = req.params.id;
    db.dan.destroy({
      where: { id: id }
    })
      .then(deleteddan => {
        res.json(deleteddan);
      });
});

// crud tip
app.post('/v2/tip', (req, res) => {
    const postNaziv = req.body.naziv;
    db.tip.findOne({where:{naziv:postNaziv}}).then(function(tip){
        if(tip) res.send("tip " + req.body.naziv + " vec postoji");
        else db.tip.create({naziv:postNaziv}).then(function() {
            res.status(200);
            res.send("tip " + req.body.naziv + " kreiran");
        })
    })
});

app.get('/v2/tip', (req, res) => {
    db.tip.findAll().then(tipovi => {
        res.json(tipovi);
    });
});

app.get('/v2/tip/:id', (req, res) => {
    const id = req.params.id;
    db.tip.findByPk(id).then(tip => {
        res.json(tip);
      });
});

app.put('/v2/tip/:id', (req, res) => {
    const id = req.params.id;
    const updates = req.body;
    db.tip.findByPk(id).then(rez => {
        return rez.update(updates)
      }).then(updatedtip => {
        res.json(updatedtip);
      });
});

app.delete('/v2/tip/:id', (req, res) => {
    const id = req.params.id;
    db.tip.destroy({
      where: { id: id }
    })
      .then(deletedtip => {
        res.json(deletedtip);   
      });
});

// crud grupa

app.post('/v2/grupa', (req, res) => {
    const postNaziv = req.body.naziv;
    db.grupa.findOne({where:{naziv:postNaziv, predmetId:req.body.predmetId}}).then(function(grupa){
        if(grupa) res.send("grupa " + req.body.naziv + " vec postoji");
        else db.grupa.create(req.body).then(function() {
            res.status(200);
            res.send("grupa " + req.body.naziv + " kreiran");
        })
    })
});

app.get('/v2/grupa', (req, res) => {
    db.grupa.findAll().then(grupe => {
        res.json(grupe);
    });
});

app.get('/v2/grupa/:id', (req, res) => {
    const id = req.params.id;
    db.grupa.findByPk(id).then(grupa => {
        res.json(grupa);
      });
});

app.put('/v2/grupa/:id', (req, res) => {
    const id = req.params.id;
    const updates = req.body;
    db.grupa.findByPk(id).then(rez => {
        return rez.update(updates)
      }).then(updatedgrupa => {
        res.json(updatedgrupa);
      });
});

app.delete('/v2/grupa/:id', (req, res) => {
    const id = req.params.id;
    db.grupa.destroy({
      where: { id: id }
    })
      .then(deletedgrupa => {
        res.json(deletedgrupa);
      });
});

// crud aktivnost
app.post('/v2/aktivnost', (req, res) => {
    const postAktivnsot = req.body;
    db.aktivnost.findOne({where:{naziv:postAktivnsot.naziv, 
                                pocetak:postAktivnsot.pocetak, 
                                kraj:postAktivnsot.kraj, 
                                danId:postAktivnsot.danId,
                                tipId:postAktivnsot.tipId, 
                                predmetId:postAktivnsot.predmetId, 
                                grupaId:postAktivnsot.grupaId}}).then(function(aktivnost){
        if(aktivnost) res.send("aktivnost " + req.body.naziv + " vec postoji");
        else db.aktivnost.create(req.body).then(function() {
            res.status(200);
            res.send("aktivnost " + req.body.naziv + " kreiran");
        })
    })
});

app.get('/v2/aktivnost', (req, res) => {
    db.aktivnost.findAll().then(aktivnosti => {
        res.json(aktivnosti);
    });
});

app.get('/v2/aktivnost/:id', (req, res) => {
    const id = req.params.id;
    db.aktivnost.findByPk(id).then(aktivnost => {
        res.json(aktivnost);
      });
});

app.put('/v2/aktivnost/:id', (req, res) => {
    const id = req.params.id;
    const updates = req.body;
    db.aktivnost.findByPk(id).then(rez => {
        return rez.update(updates)
      }).then(updatedaktivnost => {
        res.json(updatedaktivnost);
      });
});

app.delete('/v2/aktivnost/:id', (req, res) => {
    const id = req.params.id;
    db.aktivnost.destroy({
      where: { id: id }
    })
      .then(deletedaktivnost => {
        res.json(deletedaktivnost);
      });
});

// crud student

app.post('/v2/student', (req, res) => {
    db.student.findOne({where: {ime:req.body.ime, index:req.body.index}}).then(student => {
        if(student) {
            db.grupa.findOne({where: {id: req.body.grupaId}}).then( grupa => {
                db.grupa.findAll({where: {predmetId: grupa.predmetId}}).then( grupe => {
                    student.getStudentiGrupe().then( rez => {
                        var grPostoji = false;
                        var noveGr =  [];
                        for ( var i = 0; i<rez.length; i++) {
                            if(rez[i].predmetId == grupa.predmetId && rez[i].naziv != grupa.naziv) {
                                grPostoji = true;
                            } else {
                                noveGr.push(rez[i]);
                            }
                        }
                        if(grPostoji) {
                            noveGr.push(grupa);
                            student.setStudentiGrupe(noveGr);
                        } else {
                            noveGr.push(grupa);
                            student.setStudentiGrupe(noveGr);
                        }
                        res.status(200).send("Sve je ok");
                    })
                })
            })
        } else {
            db.student.findOne( {where:{index:req.body.index}}).then( student => {
                if(student) res.status(400).send("Student " + req.body.ime + " nije kreiran jer postoji student " + student.ime + " sa istim indexom " + student.index);
                else db.student.create(req.body).then(function(student) {
                    student.addStudentiGrupe([req.body.grupaId]);
                    res.status(200).send("Sve je ok");
                })
            })
        }
    })
})

app.get('/v2/student', (req, res) => {
    db.student.findAll().then(studenti => {
        res.json(studenti);
    });
});

app.get('/v2/student/:id', (req, res) => {
    const id = req.params.id;
    db.student.findByPk(id).then(student => {
        res.json(student);
      });
});

app.put('/v2/student/:id', (req, res) => {
    const id = req.params.id;
    const updates = req.body;
    db.student.findByPk(id).then(rez => {
        return rez.update(updates)
      }).then(updatedstudent => {
        res.json(updatedstudent);
      });
});

app.delete('/v2/student/:id', (req, res) => {
    const id = req.params.id;
    db.studentGrupa.destroy({
        where: {studentId: id}
    }).then( () => {
        db.student.destroy({
            where: { id: id }
          })
            .then(deletedstudent => {
              res.json(deletedstudent);
            });
    });
});

app.listen(port, () => {
    console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
  });